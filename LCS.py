def LCS(n, m):
    if (n < 0 or m < 0):
        return 0
    elif s1[n] == s2[m]:
        return 1 + LCS(n-1, m-1)
    else:
        return max(LCS(n-1, m), LCS(n, m-1))
