# -*- coding: utf-8 -*-
"""
Created on Wed Apr  8 23:28:27 2015

@author: Sisi_2
"""
import ast
from toposort import toposort_flatten

INPUTCODE = "fib"
INPUTVAL = 100

DPTable = {}

baseCaseHeaderCode = """
def header_checkBaseCase(n):
    if (true):
        return True
    else:
        return False   

BaseCases.append(header_checkBaseCase(5))
"""

dependencyHeaderCode = """
def header_getDependency(n):
    return (n)

Dependencies.append(header_getDependency(5))
"""

dpWrapperHeaderCoder = """
def DP(n):
    return DPTable[n]
"""

with open (INPUTCODE + ".py", "r") as myfile:
    source = myfile.read()

###################################
#  Get base cases
###################################

BaseCases = []

def checkIfBaseCase(valToCheck, recursiveCode):
    BaseCaseChecker(valToCheck).visit(recursiveCode)

class BaseCaseChecker(ast.NodeVisitor):
    def __init__(self, valToCheck):
        self.valToCheck = valToCheck
        
    def visit_If(self, if_node):
        checkFunc = ast.parse(baseCaseHeaderCode, mode='exec')
        ReplaceCompInCheckFunc(if_node.test).visit(checkFunc)
        ReplaceVarToCheck(self.valToCheck).visit(checkFunc)
        code = compile(checkFunc, "<ast>", mode='exec')
        exec(code)

class ReplaceCompInCheckFunc(ast.NodeTransformer):
    def __init__(self, compOp):
        self.compOp = compOp
    
    def visit_If(self, if_node):
        newIf = if_node
        newIf.test = self.compOp
        ast.fix_missing_locations(newIf)
        return newIf        

class ReplaceVarToCheck(ast.NodeVisitor):
    def __init__(self, valToCheck):
        self.valToCheck = valToCheck
    
    def visit_Call(self, append_call_node):
        ReplaceVarToCheckHelper(self.valToCheck).visit(append_call_node.args[0])

class ReplaceVarToCheckHelper(ast.NodeTransformer):
    def __init__(self, valToCheck):
        self.valToCheck = valToCheck
    
    def visit_Call(self, call_node):
        args = [ast.Num(self.valToCheck)]
        newCall = call_node
        newCall.args = args
        ast.fix_missing_locations(newCall)
        return newCall
        
###################################
#  Get dependencies
###################################
        
Dependencies = []        

def getDependencies(valToCheck, recursiveCode):
    VisitRecurse(valToCheck).visit(recursiveCode)        

class VisitRecurse(ast.NodeVisitor):
    def __init__(self, valToCalc):
        self.valToCalc = valToCalc
        
    def visit_Call(self, call_node):
        self.generic_visit(call_node)
        if call_node.func.id == INPUTCODE:
            getDepFunc = ast.parse(dependencyHeaderCode, mode='exec')
            ReplaceDependency(call_node.args[0]).visit(getDepFunc)
            ReplaceVarToCheck(self.valToCalc).visit(getDepFunc)
            code = compile(getDepFunc, "<ast>", mode='exec')
            exec(code)
        
class ReplaceDependency(ast.NodeTransformer):
    def __init__(self, calcOp):
        self.calcOp = calcOp
    
    def visit_Return(self, returnStmt):
        newReturn = returnStmt
        newReturn.value = self.calcOp
        ast.fix_missing_locations(newReturn)
        return newReturn

###################################
#  Massage original function
###################################
class VisitFunction(ast.NodeTransformer):
    def visit_FunctionDef(self, func_node):
        ReplaceRecurse().visit(func_node.body[0])
        return func_node
    
class ReplaceRecurse(ast.NodeTransformer):
    def visit_Call(self, node):
        self.generic_visit(node)
        if node.func.id == INPUTCODE:
            node.func = ast.Name(id='DP', ctx=ast.Load())                
            ast.fix_missing_locations(node)
            return node
        else:
            return node


###################################
#  Executing things
###################################

# Load recursive function
recursiveCode = ast.parse(source, mode='exec')
# Create graph
graph = {}

# Get basecases
for x in range(INPUTVAL + 1):
    checkIfBaseCase(x, recursiveCode)

BaseCasesList = []

for i in range(len(BaseCases)):
    if BaseCases[i]:
        BaseCasesList.append(i)
    else:
        Dependencies = []
        getDependencies(i, recursiveCode)
        graph[i] = set(Dependencies)

# Topo sort dependency graph
evalOrder = BaseCasesList + (list(toposort_flatten(graph)))

dpWrapperFunc = ast.parse(dpWrapperHeaderCoder, mode='exec')
exec(compile(dpWrapperFunc, "<ast>", mode='exec'))

# Rewrite original function
VisitFunction().visit(recursiveCode)

# Execute modified code
exec(compile(recursiveCode, "<ast>", mode='exec'))

for i in evalOrder:
    DPTable[i] = locals()["fib"](i)

print(locals()["fib"](INPUTVAL))



