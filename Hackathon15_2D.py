# -*- coding: utf-8 -*-
"""
Created on Wed Apr  8 23:28:27 2015

@author: Sisi_2
"""
import ast
from itertools import product
from toposort import toposort_flatten

#inputs for LCS
INPUTCODE = "LCS"
s1 = "hello this is a test string"
s2 = "thelse strings are way too long to do by naive recursion"


V1 = len(s1)-1 #these are INDICES!
V2 = len(s2)-1 #these are INDICES!

DPTable = {}

baseCaseHeaderCode = """
def header_checkBaseCase(n,m):
    if (true):
        return True
    else:
        return False   

BaseCases.append(header_checkBaseCase(5,5))
"""

dependencyHeaderCode = """
def header_getDependency(n,m):
    return (stuff)

Dependencies.append(header_getDependency(5,5))
"""

dpWrapperHeaderCoder = """
def DP(n,m):
    return DPTable[(n,m)]
"""

with open (INPUTCODE + ".py", "r") as myfile:
    source = myfile.read()

###################################
#  Get base cases
###################################

BaseCases = []

def checkIfBaseCase(valToCheck, recursiveCode):
    BaseCaseChecker(valToCheck).visit(recursiveCode)

class BaseCaseChecker(ast.NodeVisitor):
    def __init__(self, valToCheck):
        self.valToCheck = valToCheck
        
    def visit_If(self, if_node):
        checkFunc = ast.parse(baseCaseHeaderCode, mode='exec')
        ReplaceCompInCheckFunc(if_node.test).visit(checkFunc)
        ReplaceVarToCheck(self.valToCheck).visit(checkFunc)
        code = compile(checkFunc, "<ast>", mode='exec')
        exec(code)

class ReplaceCompInCheckFunc(ast.NodeTransformer):
    def __init__(self, compOp):
        self.compOp = compOp
    
    def visit_If(self, if_node):
        newIf = if_node
        newIf.test = self.compOp
        ast.fix_missing_locations(newIf)
        return newIf        

class ReplaceVarToCheck(ast.NodeVisitor):
    def __init__(self, valToCheck):
        self.valToCheck = valToCheck
    
    def visit_Call(self, append_call_node):
        ReplaceVarToCheckHelper(self.valToCheck).visit(append_call_node.args[0])

class ReplaceVarToCheckHelper(ast.NodeTransformer):
    def __init__(self, valToCheck):
        self.valToCheck = valToCheck
    
    def visit_Call(self, call_node):
        n = ast.Num(self.valToCheck[0])
        m = ast.Num(self.valToCheck[1])
        args = [n,m]
        newCall = call_node
        newCall.args = args
        ast.fix_missing_locations(newCall)
        return newCall
        
###################################
#  Get dependencies
###################################
        
Dependencies = []        

def getDependencies(valToCheck, recursiveCode):
    VisitRecurse(valToCheck).visit(recursiveCode)        

class VisitRecurse(ast.NodeVisitor):
    def __init__(self, valToCalc):
        self.valToCalc = valToCalc
        
    def visit_Call(self, call_node):
        self.generic_visit(call_node)
        if call_node.func.id == INPUTCODE:
            getDepFunc = ast.parse(dependencyHeaderCode, mode='exec')
            ReplaceDependency(call_node.args[0], call_node.args[1]).visit(getDepFunc)
            ReplaceVarToCheck(self.valToCalc).visit(getDepFunc)
            code = compile(getDepFunc, "<ast>", mode='exec')
            exec(code)
        
class ReplaceDependency(ast.NodeTransformer):
    def __init__(self, nCalcOp, mCalcOp):
        self.nCalcOp = nCalcOp
        self.mCalcOp = mCalcOp
    
    def visit_Return(self, returnStmt):
        newReturn = returnStmt
        returnVal = ast.Tuple(elts=[self.nCalcOp, self.mCalcOp], ctx=ast.Load())
        newReturn.value = returnVal
        ast.fix_missing_locations(newReturn)
        return newReturn

###################################
#  Massage original function
###################################
class VisitFunction(ast.NodeTransformer):
    def visit_FunctionDef(self, func_node):
        ReplaceRecurse().visit(func_node.body[0])
        return func_node
    
class ReplaceRecurse(ast.NodeTransformer):
    def visit_Call(self, node):
        self.generic_visit(node)
        if node.func.id == INPUTCODE:
            node.func = ast.Name(id='DP', ctx=ast.Load())                
            ast.fix_missing_locations(node)
            return node
        else:
            return node

###################################
#  Indices helpers
###################################

def nFromIndex(i):
    return int(i/(V2+1))

def mFromIndex(i):
    return i%(V2+1)

###################################
#  Executing things
###################################

# Load recursive function
recursiveCode = ast.parse(source, mode='exec')
#print(ast.dump(recursiveCode))

# Create graph
graph = {}

## Get basecases
for (n, m) in product(list(range(V1+1)), list(range(V2+1))):    
    checkIfBaseCase((n,m), recursiveCode)

#print(BaseCases)
BaseCasesList = []

for i in range(len(BaseCases)):
    n = nFromIndex(i)
    m = mFromIndex(i)
    args = (n,m)
    if BaseCases[i]:
        BaseCasesList.append(args)
    else:
        Dependencies = []
        getDependencies(args, recursiveCode)
        graph[args] = set(Dependencies)

## Topo sort dependency graph
evalOrder = BaseCasesList + (list(toposort_flatten(graph)))
#print(evalOrder)

dpWrapperFunc = ast.parse(dpWrapperHeaderCoder, mode='exec')
exec(compile(dpWrapperFunc, "<ast>", mode='exec'))

# Rewrite original function
VisitFunction().visit(recursiveCode)

# Execute modified code
exec(compile(recursiveCode, "<ast>", mode='exec'))

for (n,m) in evalOrder:
    DPTable[(n,m)] = locals()[INPUTCODE](n,m)

print(locals()[INPUTCODE](V1,V2))



